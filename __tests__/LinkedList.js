const { LinkedList } = require('../data-structures/LinkedList');

let ll = new LinkedList();

describe('Testing LinkedList', () => {
  it('Should append to head when empty', () => {
    ll.append(2);
    expect(ll.head).toEqual({ val: 2, next: null });
  });

  it('Should prepend to head when empty', () => {
    ll.reset();
    ll.prepend(3);
    expect(ll.head).toEqual({ val: 3, next: null });
  });

  it('Should prepend properly', () => {
    ll.prepend(2);
    expect(ll.head).toEqual({ val: 2, next: { val: 3, next: null } });
  });

  it('Show append properly', () => {
    ll.reset();
    ll.append(1);
    ll.append(3);
    ll.append(4);
    expect(ll.head.next).toEqual({ val: 3, next: { val: 4, next: null } });
  });
});
