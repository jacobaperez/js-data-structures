const { Stack } = require('../data-structures/Stack-LinkedList');

describe('Stack Tests:', () => {
  let stack = new Stack();

  it('Should return true for empty stack', () => {
    expect(stack.isEmpty()).toBe(true);
  });

  it('Should return false for non-empty stack', () => {
    stack.push(1);
    expect(stack.isEmpty()).toBe(false);
  });

  it('Should peek correctly', () => {
    stack.push(2);
    expect(stack.peek()).toBe(2);
  });

  it('Should pop correctly', () => {
    stack.pop();
    let popped = stack.pop();
    expect(popped).toEqual(1);
  });

  it('Should catch empty stack pop', () => {
    expect(stack.pop()).toEqual('Empty Stack');
  });
});
