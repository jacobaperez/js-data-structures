const { DoublyLinkedList } = require('../data-structures/DoublyLinkedList');

let ll = new DoublyLinkedList();

describe('Testing DoublyLinkedList', () => {
  it('Should append to head when empty', () => {
    ll.append(2);
    expect(ll.head).toEqual({ val: 2, next: null, prev: null });
  });

  it('Should prepend to head when empty', () => {
    ll.reset();
    ll.prepend(3);
    expect(ll.head).toEqual({ val: 3, next: null, prev: null });
  });

  it('Should prepend properly', () => {
    ll.prepend(2);
    expect(ll.head.next.prev).toEqual(ll.head);
  });

  it('Should prepend properly', () => {
    expect(ll.tail.prev).toEqual(ll.head);
  });

  it('Show append properly', () => {
    ll.reset();
    ll.append(1);
    ll.append(2);
    ll.append(3);
    expect(ll.printForward()).toEqual('1->2->3->null');
  });

  it('Show append properly', () => {
    expect(ll.printBackward()).toEqual('3->2->1->null');
  });
});
