const { Queue } = require('../data-structures/Queue-LinkedList');

describe('Queue Tests:', () => {
  let queue = new Queue();

  it('Should return true for empty queue', () => {
    expect(queue.isEmpty()).toBe(true);
  });

  it('Should return false for non-empty queue', () => {
    queue.enqueue(1);
    expect(queue.isEmpty()).toBe(false);
  });

  it('Should peek correctly', () => {
    queue.enqueue(2);
    expect(queue.peek()).toBe(1);
  });

  it('Should dequeue correctly', () => {
    queue.dequeue();
    let popped = queue.dequeue();
    expect(popped).toEqual(2);
  });

  it('Should catch empty queue dequeue', () => {
    expect(queue.dequeue()).toEqual('Empty Queue');
  });
});
