/**
 * Node for Singly Linked List
 */
class Node {
  constructor(val) {
    this.val = val;
    this.next = null;
  }
}

/**
 * Linked List
 */
class LinkedList {
  constructor() {
    this.head = null;
    this.tail = null;
  }

  /**
   *
   * @param {Number} val - Value to inserted
   * at the end of the linked list
   */
  append(val) {
    let newNode = new Node(val);
    if (!this.head) {
      this.head = newNode;
      this.tail = this.head;
    } else {
      this.tail.next = newNode;
      this.tail = this.tail.next;
    }
  }

  /**
   *
   * @param {Number} val - Value to be inserted
   * at the beginning of the linked list
   */
  prepend(val) {
    let newNode = new Node(val);
    if (!this.head) {
      this.head = newNode;
      this.tail = this.head;
    } else {
      newNode.next = this.head;
      this.head = newNode;
    }
  }

  removeNode(val) {
    if (!this.head) {
      return 'List is Empty'
    } 
    if (this.head.val === val) {
      this.head = this.head.next;
      return 'Success'
    }
    let curr = this.head;
    while (curr.next && curr.next.val !== val) {
      curr = curr.next;
    }
    // Handle stuff here.

  }

  /**
   * Removes everything, resets the linked list
   */
  reset() {
    this.head = null;
    this.tail = this.head;
  }
}

module.exports = {
  Node,
  LinkedList,
};
