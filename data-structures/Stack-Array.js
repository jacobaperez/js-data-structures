/**
 * Stack data structure using and Array
 */
class Stack {
  constructor() {
    this.stack = [];
  }

  isEmpty() {
    return this.stack.length === 0;
  }

  peek() {
    if (this.isEmpty()) {
      return 'Empty Stack';
    }
    return this.stack[this.stack.length - 1];
  }

  pop() {
    if (this.isEmpty()) {
      return 'Empty Stack';
    }
    return this.stack.pop();
  }

  push(val) {
    this.stack.push(val);
  }
}

exports.Stack = Stack;