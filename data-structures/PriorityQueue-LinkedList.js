class Node {
  constructor(val, priority = 0) {
    this.val = val;
    this.priority = priority;
    this.next = null;
  }
}

class PriorityQueue {
  constructor() {
    this.queue = null;
  }

  /**
   * O(1) - lookup time
   */
  isEmpty() {
    return this.queue === null;
  }

  /**
   * O(n) - insertion time
   * @param {Number} val
   * @param {Number} priority
   */
  insert(val, priority) {
    let node = new Node(val, priority);
    //Check if queue is empty
    if (!this.queue) {
      this.queue = node;
    } else {
      let curr = this.queue;
      // check if priority is higher than highest
      if (priority > curr.priority) {
        let newHead = node;
        newHead.next = curr;
        this.queue = newHead;
      } else {
        // iterate through queue until priority is at proper location
        while (curr.next && curr.next.priority >= priority) {
          curr = curr.next;
        }
        // check if you've reached end of queue
        if (!curr.next) {
          curr.next = node;
        } else {
          // Insert node correctly
          let rest = curr.next;
          curr.next = node;
          curr.next.next = rest;
        }
      }
    }
  }

  /**
   * O(1) - Lookup
   */
  getHighestPriority() {
    if (!this.queue) {
      return 'Empty Queue';
    }
    return this.queue.val;
  }

  dequeue() {
    if (!this.queue) {
      return 'Empty Queue';
    }
    let value = this.queue.val;
    this.queue = this.queue.next;
    return value;
  }

  printQueue() {
    if (!this.queue) {
      return 'Empty Queue';
    }
    let string = '';
    let curr = this.queue;
    while (curr !== null) {
      string += `[val:${curr.val}, priority:${curr.priority}]\n`;
      curr = curr.next;
    }
    console.log(string)
    // return string;
  }
}
