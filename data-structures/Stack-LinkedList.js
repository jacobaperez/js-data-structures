const { LinkedList } = require('./LinkedList');

class Stack {
  constructor() {
    this.stack = new LinkedList();
    this.size = 0;
  }

  isEmpty() {
    return this.stack.head === null;
  }

  peek() {
    if (this.isEmpty()) {
      return 'Empty Stack';
    }
    return this.stack.head.val;
  }

  pop() {
    if (this.isEmpty()) {
      return 'Empty Stack';
    }
    let val = this.stack.head.val;
    this.stack.head = this.stack.head.next;
    return val;
  }

  push(val) {
    this.stack.prepend(val);
  }
}

exports.Stack = Stack;