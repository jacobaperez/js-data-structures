/**
 * Queue data structure made with Array.
 */
class Queue {
  constructor() {
    this.queue = [];
  }

  isEmpty() {
    return this.queue.length === 0;
  }

  peek() {
    if (this.isEmpty()) {
      return 'Empty Queue';
    }
    return this.queue[0];
  }

  enqueue(val) {
    this.queue.push(val);
  }

  dequeue() {
    if (this.isEmpty()) {
      return 'Empty Queue';
    }
    return this.queue.shift();
  }
}

exports.Queue = Queue;
