class Node {
  constructor(val) {
    this.val = val;
    this.next = null;
    this.prev = null;
  }
}

class DoublyLinkedList {
  constructor() {
    this.head = null;
    this.tail = null;
  }

  append(val) {
    let node = new Node(val);
    if (!this.head) {
      this.head = node;
      this.tail = this.head;
    } else {
      this.tail.next = node;
      node.prev = this.tail;
      this.tail = node;
    }
  }

  prepend(val) {
    let node = new Node(val);
    if (!this.head) {
      this.head = node;
      this.tail = this.head;
    } else {
      node.next = this.head;
      this.head.prev = node;
      this.head = node;
    }
  }

  shift() {
    if (!this.head) {
      return 'Empty List';
    }
    if (this.head === this.tail) {
      let val = this.head.val;
      this.reset();
      return val;
    } else {
      let val = this.head.val;
      this.head = this.head.next;
      this.head.prev = null;
      return val;
    }
  }

  pop() {
    if (!this.head) {
      return 'Empty List';
    }
    if (this.head === this.tail) {
      let val = this.head.val;
      this.reset();
      return val;
    } else {
      let val = this.tail.val;
      this.tail = this.tail.prev;
      this.tail.next = null;
      return val;
    }
  }

  printForward() {
    if (!this.head) {
      return 'Empty List';
    } else {
      let curr = this.head;
      let val = '';
      while (curr !== null) {
        val += `${curr.val}->`;
        curr = curr.next;
      }
      val += 'null';
      return val;
    }
  }

  printBackward() {
    if (!this.head) {
      return 'Empty List';
    } else {
      let curr = this.tail;
      let val = '';
      while (curr !== null) {
        val += `${curr.val}->`;
        curr = curr.prev;
      }
      val += 'null';
      return val;
    }
  }

  reset() {
    this.head = null;
    this.tail = this.head;
  }
}

exports.DoublyLinkedList = DoublyLinkedList;
