const { LinkedList } = require('./LinkedList');

/**
 * Queue data structure with a linkedlist
 */
class Queue {
  constructor() {
    this.queue = new LinkedList();
  }

  isEmpty() {
    return this.queue.head === null;
  }

  peek() {
    if (this.isEmpty()) {
      return 'Empty Queue';
    }
    return this.queue.head.val;
  }

  enqueue(val) {
    this.queue.append(val);
  }

  dequeue() {
    if (this.isEmpty()) {
      return 'Empty Queue';
    }
    let val = this.queue.head.val;
    this.queue.head = this.queue.head.next;
    return val;
  }
}

exports.Queue = Queue;
