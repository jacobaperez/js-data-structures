/**
 * Max Heap Data Structure
 */
class MaxHeap {
  constructor() {
    this.heap = [];
  }

  isEmpty() {
    return this.heap.length === 0;
  }

  insert(val) {
    this.heap.push(val);
    this._heapifyUp();
  }

  peek() {
    if (this.isEmpty()) {
      return 'Empty Heap';
    }
    return this.heap[0];
  }

  getMax() {
    if (this.isEmpty()) {
      return 'Empty Heap';
    }
    let value = this.heap[0];
    this.heap[0] = this.heap.pop();
    this._heapifyDown();
    return value;
  }

  _getLeftChildIndex(i) {
    return 2 * i + 1;
  }

  _getRightChildIndex(i) {
    return 2 * i + 2;
  }

  _getParentIndex(i) {
    if (i > 0) {
      return Math.floor((i - 1) / 2);
    }
    return -1;
  }

  _heapifyUp() {
    let nodeValue = this.heap.length - 1;
    let parentIndex = this._getParentIndex(nodeValue);
    while (
      parentIndex !== -1 &&
      this.heap[parentIndex] < this.heap[nodeValue]
    ) {
      this._swap(parentIndex, nodeValue);
      nodeValue = parentIndex;
      parentIndex = this._getParentIndex(nodeValue);
    }
  }

  _heapifyDown() {
    let nodeValue = 0;
    let leftChildIndex = this._getLeftChildIndex(nodeValue);

    while (leftChildIndex <= this.heap.length) {
      let biggerIndex = leftChildIndex;
      let rightChildIndex = this._getRightChildIndex(nodeValue);
      if (
        rightChildIndex <= this.heap.length &&
        this.heap[rightChildIndex] > this.heap[leftChildIndex]
      ) {
        biggerIndex = rightChildIndex;
      }

      if (this.heap[nodeValue] >= this.heap[biggerIndex]) {
        break;
      }
      this._swap(biggerIndex, nodeValue);
      nodeValue = biggerIndex;
      leftChildIndex = this._getLeftChildIndex(nodeValue);
    }
  }

  _swap(i, j) {
    let temp = this.heap[i];
    this.heap[i] = this.heap[j];
    this.heap[j] = temp;
  }
}
